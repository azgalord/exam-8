import React from 'react';
import { Link } from 'react-router-dom';
import EditBtn from '../../../assets/img/edit.svg';
import DeleteBtn from '../../../assets/img/clear-button.svg';

import './Quote.css';

const Quote = props => {
  return (
    <div className="Quote">
      <div className="QuoteText">
        <p>{props.text}</p>
        <h3>{props.author}</h3>
      </div>
      <div className="QuoteButtons">
        <Link onClick={props.edit} style={{backgroundImage: `url(${EditBtn})`}} to={"/quotes/" + props.id + '/edit/'}/>
        <button onClick={props.delete} style={{backgroundImage: `url(${DeleteBtn})`}} type="button"/>
      </div>
    </div>
  );
};

export default Quote;
