import React from 'react';
import Quote from "./Quote/Quote";

import './QuotesList.css';

const QuotesList = props => {
  return (
    <div className="QuotesList">
      {props.quotes.map((quote, index) => (
        <Quote
          edit={() => props.edit(quote.id)}
          id={quote.id}
          delete={() => props.delete(quote.id)}
          key={index}
          text={quote.text}
          author={quote.author}
        />
      ))}
    </div>
  );
};

export default QuotesList;
