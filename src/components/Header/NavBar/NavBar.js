import React from 'react';
import { NavLink } from 'react-router-dom';

import './NavBar.css';

const NavBar = () => {
  return (
    <nav className="NavBar">
      <ul>
        <li>
          <NavLink to="/">Quotes</NavLink>
        </li>
        <li>
          <NavLink to="/add-quote">Submit new quote</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
