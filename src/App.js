import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AddPage from "./containers/AddPage/AddPage";
import Header from "./components/Header/Header";
import Home from "./containers/Home/Home";
import Edit from "./containers/Edit/Edit";

import './App.css';

class App extends Component {
  state = {

  };

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Fragment>
            <Header/>
            <Switch>
              <Route path="/" exact component={Home}/>
              <Route path="/quotes/:url" exact component={Home}/>
              <Route path="/add-quote/" exact component={AddPage}/>
              <Route path="/quotes/:id/edit/" component={Edit}/>
            </Switch>
          </Fragment>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
