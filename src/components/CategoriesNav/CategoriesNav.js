import React from 'react';
import { Link } from 'react-router-dom';

import './CategoriesNav.css';

const CategoriesNav = props => {
  return (
    <div className="CategoriesNav">
      <ul>
        {props.categories.map(category => (
          <Link
            onClick={() => props.onClick(category.name)}
            key={category.name}
            to={category.name === 'All' ? '/' : '/quotes/' + category.url}
          >
            {category.name}
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default CategoriesNav;
