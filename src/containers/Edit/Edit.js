import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import Form from "../../components/Form/Form";
import axios from '../../axios-quotes';

class Edit extends Component {
  state = {
    quote: {
      category: '',
      author: '',
      text: ''
    },
    categories: ['StarWars', 'StandUp', 'Business', 'Buddhism', 'SteveJobs']
  };

  getId = () => {
    const id = this.props.match.params.id;
    return '/quotes/' + id + '.json';
  };

  componentDidMount() {
    axios.get(this.getId()).then(response => {
      this.setState({quote: response.data});
    })
  }

  changeValue = (event) => {
    const name = event.target.name;
    let quote = {...this.state.quote};
    quote[name] = event.target.value;
    this.setState({quote});
  };

  changeQuote = (event) => {
    event.preventDefault();
    axios.put(this.getId(), this.state.quote).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
      <div className="Edit">
        <Container>
          <h2>Edit a quote</h2>
          <Form
            categories={this.state.categories}
            author={this.state.quote.author}
            text={this.state.quote.text}
            onChange={(event) => this.changeValue(event)}
            buttonText="Save" submit={(event) => this.changeQuote(event)}
          />
        </Container>
      </div>
    );
  }
}

export default Edit;
