import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import CategoriesNav from "../../components/CategoriesNav/CategoriesNav";
import QuotesList from "../../components/QuotesList/QuotesList";
import axios from '../../axios-quotes';

import './Home.css';

class Home extends Component {
  state = {
    categories: [
      {name: 'All', url: ''},
      {name: 'StarWars', url: 'starwars'},
      {name: 'StandUp', url: 'standup'},
      {name: 'Business', url: 'business'},
      {name: 'Buddhism', url: 'buddhism'},
      {name: 'SteveJobs', url: 'stevejobs'}
    ],
    quotes: []
  };

  getRequest = (requestUrl) => {
    axios.get(requestUrl).then(response => {
      const quotes = Object.values(response.data);
      for (let key in quotes) {
        quotes[key].id = Object.keys(response.data)[key];
      }
      this.setState({quotes});
    });
  };

  componentDidMount() {
    this.getRequest('/quotes.json');
  }

  changeContentByCategory = (url) => {
    if (url === 'All') {
      this.getRequest('/quotes.json');
    }
    this.getRequest('/quotes.json?orderBy="category"&equalTo="' + url + '"');
  };

  deletePost = (id) => {
    axios.delete('/quotes/' + id + '.json').then(() => {
      this.getRequest('/quotes.json');
      this.props.history.replace('/');
    })
  };

  render() {
    return (
      <div className="Home">
        <Container>
          <CategoriesNav
            onClick={this.changeContentByCategory}
            categories={this.state.categories}
          />
          <QuotesList delete={this.deletePost} quotes={this.state.quotes}/>
        </Container>
      </div>
    );
  }
}

export default Home;
