import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://quotes-azamat.firebaseio.com/'
});

export default instance;
