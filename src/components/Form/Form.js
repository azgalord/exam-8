import React from 'react';

import './Form.css';

const Form = props => {
  return (
    <div className="Form">
      <form>
        <label htmlFor="categories">Category</label>
        <select onChange={props.onChange} id="categories" name="category">
          {props.categories.map((category, id) => (
            <option key={id}>{category}</option>
          ))}
        </select>

        <label htmlFor="author">Author</label>
        <input
          onChange={props.onChange}
          value={props.author} id="author"
          type="text" name="author"
        />

        <label htmlFor="text">Quote text</label>
        <textarea id="text"
                  onChange={props.onChange}
                  value={props.text} name="text"
                  cols="30" rows="10"
        />
        <button onClick={props.submit} type="submit">{props.buttonText}</button>
      </form>
    </div>
  );
};

export default Form;
