import React, {Component} from 'react';
import Form from "../../components/Form/Form";
import Container from "../../components/Container/Container";
import axios from '../../axios-quotes';

class AddPage extends Component {
  state = {
    quote: {
      category: 'StarWars',
      author: '',
      text: ''
    },
    categories: ['StarWars', 'StandUp', 'Business', 'Buddhism', 'SteveJobs']
  };

  changeValue = (event) => {
    const name = event.target.name;
    let quote = {...this.state.quote};
    quote[name] = event.target.value;
    this.setState({quote});
  };

  saveNewQuote = (event) => {
    event.preventDefault();
    if (this.state.quote.author && this.state.quote.text) {
      axios.post('/quotes.json', this.state.quote)
        .finally(() => {
          const quote = {...this.state.quote};
          quote.author = '';
          quote.text = '';
          this.setState({quote});
        })
    } else {
      alert('Fields are empty!');
    }
  };

  render() {
    return (
      <div className="AddPage">
        <Container>
          <h2>Submit new quotes</h2>
          <Form
            categories={this.state.categories}
            author={this.state.quote.author}
            text={this.state.quote.text}
            onChange={(event) => this.changeValue(event)}
            buttonText="Save" submit={(event) => this.saveNewQuote(event)}
          />
        </Container>
      </div>
    );
  }
}

export default AddPage;
