import React from 'react';
import Container from "../Container/Container";
import NavBar from "./NavBar/NavBar";

import './Header.css';

const Header = () => {
  return (
    <div className="Header">
      <Container>
        <h1>Quotes Central</h1>
        <NavBar/>
      </Container>
    </div>
  );
};

export default Header;
